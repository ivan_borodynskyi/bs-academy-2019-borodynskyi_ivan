const assert = require('assert');
const { URL } = require('url');

describe('HEDONIST', () => {

    it('invalid data to login', () => {

        browser.url("https://staging.bsa-hedonist.online");
        const email_field = $('input[name=email]');
        const pass_field = $('input[type=password]');
        const login_btn = $('button.is-rounded');

        email_field.setValue('q');
        pass_field.setValue('q');
        login_btn.click();
        $('div.toast.is-danger div').waitForExist(3000);
        const isExist = $('div.toast.is-danger div');
        assert.equal(isExist.getText(), 'The email or password is incorrect'); //перевірка

        // const actualUrl = browser.getUrl();
        // browser.pause(1000);
        // assert.equal(actualUrl, "https://staging.bsa-hedonist.online/login");
        // browser.pause(1000);
    });
    it('should create new user', () => {
        browser.url("https://staging.bsa-hedonist.online");
        const btn = $('a.link.link-signup');
        const first_name_field = $('input[name=firstName]');
        const last_name_field = $('input[name=lastName]');
        const email_field = $('input[name=email]');
        const new_pass_field = $('input[type=password]');
        const create_btn = $('button.is-rounded');

        btn.click();
        first_name_field.setValue('Johfvnjjj');
        last_name_field.setValue('Snovbnvbnwkkk');
        email_field.setValue('test654@gmail.com'); // при повторному запуску  міняти емейл
        new_pass_field.setValue('bblassvbnbbliuii345');
        browser.pause(3000);
        create_btn.click();
        browser.pause(3000);

        const actualUrl = browser.getUrl();
        assert.equal(actualUrl, 'https://staging.bsa-hedonist.online/login'); //перевірка
    });

    it("should add new place", () => {
        browser.url('https://staging.bsa-hedonist.online');
        const emailField = $('input[name=email]');
        const passField = $('input[type=password]');
        const loginBtn = $('button.is-primary');

        emailField.setValue('test654@gmail.com'); //чомусь при запуску драйвер запускав завжди розлогованого
        passField.setValue('bblassvbnbbliuii345');
        loginBtn.click();
        browser.pause(500);

        browser.url('https://staging.bsa-hedonist.online/places/add');
        const nameField = $('input.is-medium');
        const cityField = $('div.is-expanded input[placeholder=Location]');
        const zipField = $('input[placeholder="09678"]');
        const adressField = $('input[placeholder="Khreschatyk St., 14"]');
        const phoneField = $('input[type=tel]');
        const websiteField = $('input[placeholder="http://the-best-place.com/"]');
        const descriptionField = $('textarea');
        const nextBtn = $$('span.is-success')[0];

        /*const btnArray = $$('span.is-success');
        btnArray.forEach((button) => {
        button.click();
        })*/ // напевно можна було якось перебирати кнопки масивом щоб не обявляти
        // кожен раз нову кнопку але я не розібрався як. можливо перебирати через swich

        nameField.setValue('test');
        cityField.setValue('');
        cityField.setValue('Lviv');
        browser.pause(500);
        const cityConfirm = $$('b')[0];
        cityConfirm.click();
        adressField.setValue('Main String., 15');
        phoneField.setValue('+380931234567');
        zipField.setValue('79070');
        websiteField.setValue('https://staging.bsa-hedonist.online');
        descriptionField.setValue('sadasdasdasdsadsadsadsadasdaasd');
        nextBtn.click();
        browser.pause(1000);

        const insertfile = $('input[type=file]');
        const nextBtn1 = $$('span.is-success')[1];
        insertfile.setValue('E:\\IT\\binary\\Automation\\bs-academy-2019-borodynskyi_ivan\\Test.png');
        nextBtn1.click();
        browser.pause(1000);

        const nextBtn2 = $$('span.is-success')[2];
        nextBtn2.click();
        browser.pause(1000);

        const category = $$('span.select select')[0];
        category.click();
        const bar = $$('option[value="[object Object]"]')[0];
        bar.click();
        const tag = $$('span.select select')[1];
        tag.click();
        const milk = $$('option[value="[object Object]"]')[7];
        milk.click();
        browser.pause(1000);
        const nextBtn3 = $$('span.is-success')[3];
        nextBtn3.click();
        browser.pause(1000);

        const feature = $$('span.check.is-success')[0];
        feature.click();
        browser.pause(1000);
        const nextBtn4 = $$('span.button.is-success')[4];
        nextBtn4.click();
        browser.pause(1000);

        const nextBtn5 = $$('span.button.is-success')[5];
        nextBtn5.click();
        browser.pause(1000);

        const nextBtn6 = $$('span.button.is-success')[6];
        nextBtn6.click();
        browser.pause(1000);

        const curUrl = new URL(browser.getUrl());
        const actUrl = curUrl.hostname.toString() + curUrl.pathname.toString();

        // assert.equal(actUrl, "staging.bsa-hedonist.online/place"); //перевірка (не хоче парсити не розумію чому)
    });

    it('should create list', () => {

        browser.url('https://staging.bsa-hedonist.online/my-lists');
        browser.pause(1000);
        const addNewListBtn = $('a.button.is-success');
        addNewListBtn.click();

        const listName = $('input[id=list-name]');
        listName.setValue('newlist');
        const saveBtn = $('button.button.is-success');
        saveBtn.click();
        browser.pause(1000);
        const deleteBtn = $$('button.is-danger')[0];
        browser.pause(1000);
        const isEnabled = deleteBtn.isEnabled();

        assert.equal(isEnabled, true); //перевірка
    });
    it('should delete list', () => {

        // browser.url('https://staging.bsa-hedonist.online');
        // const emailField = $('input[name=email]');
        // const passField = $('input[type=password]');
        // const loginBtn = $('button.is-primary');

        // emailField.setValue('johnsnow@john.com');
        // passField.setValue('johnjohn');
        // loginBtn.click();
        // browser.pause(1000);


        browser.url('https://staging.bsa-hedonist.online/my-lists');
        browser.pause(1000);
        const deleteBtn = $$('button.is-danger')[0];
        deleteBtn.click();
        const aproveDelBtn = $('div.box button.is-danger');
        aproveDelBtn.click();
        browser.pause(1000);
        const isEnabled = deleteBtn.isEnabled();

        assert.equal(isEnabled, false); //перевірка
    });
});