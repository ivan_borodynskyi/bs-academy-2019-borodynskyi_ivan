const AddNewPlacePage = require('./createNewPlace_po');
const page = new AddNewPlacePage();

class AddNewPlaceActions {
    enterPlaceName(value) {
        page.placeName.waitForDisplayed(7000);
        page.placeName.clearValue();
        page.placeName.setValue(value);
    }
    enterPlaceCity(value) {
        page.placeCity.waitForDisplayed(5000);
        page.placeCity.clearValue();
        page.placeCity.setValue(value);
    }
    confirmPlaceCity() {
        page.placeCityConfirm.waitForDisplayed(5000);
        page.placeCityConfirm.click();
    }
    enterPlaceAdress(value) {
        page.placeAdress.waitForDisplayed(5000);
        page.placeAdress.clearValue();
        page.placeAdress.setValue(value);
    }
    enterPlacePhone(value) {
        page.placePhone.waitForDisplayed(2000);
        page.placePhone.clearValue();
        page.placePhone.setValue(value);
    }
    enterPlaceZip(value) {
        page.placeZip.waitForDisplayed(2000);
        page.placeZip.clearValue();
        page.placeZip.setValue(value);
    }
    enterPlaceWebsite(value) {
        page.placeWebsite.waitForDisplayed(2000);
        page.placeWebsite.clearValue();
        page.placeWebsite.setValue(value);
    }
    enterPlaceDescription(value) {
        page.placeDescription.waitForDisplayed(2000);
        page.placeDescription.clearValue();
        page.placeDescription.setValue(value);
    }
    goToNextPagePhoto() {
        page.placeNextBtn0.click();
    }
    insertPhoto(path) {
        //page.placeInsertPhoto.waitForDisplayed(5000);
        browser.pause(1000);
        page.placeInsertPhoto.setValue(path);
        page.placePhoto.waitForDisplayed(3000);
    }
    goToNextPageLocation() {
        page.placeNextBtn1.waitForDisplayed(2000);
        page.placeNextBtn1.click();
    }
    goToNextPageCategories() {
        page.placeNextBtn2.waitForDisplayed(2000);
        page.placeNextBtn2.click();
    }
    selectCategoryAndTags() {
        page.placeCategoryList.waitForDisplayed(2000);
        page.placeCategoryList.click();
        page.placeSelectBar.waitForDisplayed(2000);
        page.placeSelectBar.click();
        page.placeTagList.waitForDisplayed(2000);
        page.placeTagList.click();
        page.placeSelectMilk.waitForDisplayed(2000);
        page.placeSelectMilk.click();
    }
    goToNextPageFeatures() {
        page.placeNextBtn3.waitForDisplayed(2000);
        page.placeNextBtn3.click();
    }
    selectFeatureLiveMusic() {
        //page.placeFeature.waitForDisplayed(5000);
        page.placeFeature.click();
    }
    goToNextPageCHours() {
        page.placeNextBtn4.waitForDisplayed(2000);
        page.placeNextBtn4.click();
    }
    goToNextPageAddPlace() {
        page.placeNextBtn5.waitForDisplayed(5000);
        page.placeNextBtn5.click();
    }
    confirm() {
        page.placeNextBtn6.waitForDisplayed(5000);
        page.placeNextBtn6.click();
    }
    goToNextPage() {
        browser.pause(1000);
        page.listOfBtns.waitForDisplayed(5000);
        page.placeNextBtn.isDisplayedInViewport();
        page.placeNextBtn.click();
    }
};

module.exports = new AddNewPlaceActions();