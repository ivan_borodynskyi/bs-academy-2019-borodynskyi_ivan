const menuSteps = require('../Menu/actions/menu_pa');
const pageSteps = require('./createNewPlace_pa');
const Page = require('../../helpers/helpers');
const credentials = require('../myTestdata.json');
const Validate = require('../../helpers/validators');
const Wait = require('../../helpers/waiters');
const path = require('path');

describe('create new place test', () => {
    before(() => {
        Page.loginWithDefaultUser();
        Wait.forSpinner();
    });
    it("should add new place", () => {
        menuSteps.navigateToNewPlace();
        pageSteps.enterPlaceName(credentials.placeName);
        pageSteps.enterPlaceCity(credentials.placeCity);
        pageSteps.confirmPlaceCity();
        pageSteps.enterPlaceZip(credentials.placeZip);
        pageSteps.enterPlaceAdress(credentials.placeAdress);
        pageSteps.enterPlacePhone(credentials.placePhone);
        pageSteps.enterPlaceWebsite(credentials.placeSite);
        pageSteps.enterPlaceDescription(credentials.placeDescription);
        pageSteps.goToNextPage();

        pageSteps.insertPhoto(path.join(__dirname, './test.png'));
        pageSteps.goToNextPage();

        pageSteps.goToNextPage();
        pageSteps.selectCategoryAndTags();
        pageSteps.goToNextPage();
        pageSteps.selectFeatureLiveMusic();
        pageSteps.goToNextPage();

        pageSteps.goToNextPage();

        pageSteps.goToNextPage();
        Wait.forSpinner();
        Validate.newPlaceIS(credentials.placeName);


    });
    after(() => {

        browser.reloadSession();
    })
})