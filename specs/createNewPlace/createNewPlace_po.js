class AddNewPlacePage {
    get placeName() { return $('input.is-medium') };
    get placeCity() { return $('div.control.is-expanded div.control.is-clearfix input') };
    get placeCityConfirm() { return $('.place-location div.dropdown-menu:not([style="display: none;"]') };
    get placeZip() { return $('input[placeholder="09678"]') };
    get placeAdress() { return $('input[placeholder="Khreschatyk St., 14"]') }; //знайти кращий локатор бо коли сайт загружається на іншій мові - інший плейсхолдер
    get placePhone() { return $('input[type=tel]') };
    get placeWebsite() { return $('input[placeholder="http://the-best-place.com/"]') };
    get placeDescription() { return $('textarea') };
    get placeCategoryList() { return $$('span.select select')[0] };
    get placeSelectBar() { return $$('option[value="[object Object]"]')[0] };
    get placeTagList() { return $$('span.select select')[1] };
    get placeSelectMilk() { return $$('option[value="[object Object]"]')[7] };
    get placeInsertPhoto() { return $('input[type=file]') };
    get placePhoto() { return $('img.image-preview') };

    get placeFeature() { return $$('span.is-success')[4] };


    get listOfBtns() { return $('div.tab-item:not([style="display: none;"])') };
    get placeNextBtn() { return $('div.tab-item:not([style="display: none;"]) span.button.is-success'); }


    get placeTypeAfterAdd() { return $('place-venue__category') };



};



module.exports = AddNewPlacePage;