const ListPage = require('./createAndDeleteList_po');
const page = new ListPage;
class ListActions {

    createNewList() {
        page.addNewListBtn.waitForDisplayed(3000);
        page.addNewListBtn.click();
    }
    enterListName(value) {
        page.listName.waitForDisplayed(3000);
        page.listName.clearValue();
        page.listName.setValue(value);
    }
    saveList() {
        page.saveBtn.waitForDisplayed(3000);
        page.saveBtn.click();
    }
    getAllLists() {
        page.getAllLists.waitForDisplayed(3000);
        return page.getAllLists;
    }
    getNameOfAddedList() {
        page.createdList.waitForDisplayed(3000);
        return createdList.getText();
    }
}

module.exports = new ListActions();