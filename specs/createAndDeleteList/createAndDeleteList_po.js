class ListPage {

    get addNewListBtn() { return $('a.button.is-success') };
    get listName() { return $('input[id=list-name]') };
    get saveBtn() { return $('button.button.is-success') };
    get deleteBtn() { return $$('button.is-danger')[0] };
    get createdList() { return $$('h3.title.has-text-primary a')[0] };
    get allLists() { return $$('ul>li')["length"] - 1 }
        // get isEnabled() {return $('a.button.is-success')};
        // const  = ;
        // const saveBtn = $('button.button.is-success');
        // const deleteBtn = $$('button.is-danger')[0];
        // const isEnabled = deleteBtn.isEnabled();
}
module.exports = ListPage;