const fetch = require('node-fetch'); {
    const url = 'http://165.227.137.250/api/v1'
    const args = require('../../myTestdata.json');

    class List {


        static async sendDeleteUserListRequest(url, args) {
            return fetch(`${url}/user-lists`, {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NjQ2NTMwNzksImV4cCI6MTU2NDY1NjY3OSwibmJmIjoxNTY0NjUzMDc5LCJqdGkiOiJ4UldLNjdGSUVlcUxuTHpGIiwic3ViIjozNjMsInBydiI6ImZiMTVkNjNjNmE0MzZjYjRjZWEzZDMwZTBhYjk5ZmM4YTM3NTgyM2MifQ.mZ6k0hl0-_ycm37o4Di-iXQ8XU3yJOkox8agVuBMpOQ',
                    'Content-Type': 'application/json'
                }

            });
        };
    };

    async function deleteList(url, args) {

        console.log(`List is deleting for ${url}`);
        const response = await List.sendDeleteUserList(url, args);
        if (response.status === 200) {
            console.log(`List is deleted on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully delete list for ${url} `);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
    };

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Deleting List=========');
        deleteList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });

}