const fetch = require('node-fetch'); {
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./../myTestdata.json');

    class List {
        static async sendDeleteUserListRequest(url, args) {
            return fetch(`${url}/user-lists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MFwvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NjQ2NzI2MjIsImV4cCI6MTU2NDY3NjIyMiwibmJmIjoxNTY0NjcyNjIyLCJqdGkiOiJCbGdKY2pKMExqSGlwMkxxIiwic3ViIjozNjMsInBydiI6ImZiMTVkNjNjNmE0MzZjYjRjZWEzZDMwZTBhYjk5ZmM4YTM3NTgyM2MifQ.CaP0LLaBpZMtNLWkOK9yciRHa-NPoMPCaYnftGBjH-Y'
                },
                body: JSON.stringify({
                    name: args.listname
                })
            });
        };
    }

    async function createList(url, args) {
        console.log(`List is creating for ${url}`);
        const response = await List.sendDeleteUserList(url, args);
        if (response.status === 201) {
            console.log(`List is created on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully create list for ${url} `);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
    };

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Creating List=========');
        createList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });

}