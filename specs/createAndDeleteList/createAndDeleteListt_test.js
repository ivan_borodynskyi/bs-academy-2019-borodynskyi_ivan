const menuSteps = require('../Menu/actions/menu_pa');
const pageSteps = require('./createAndDeleteList_pa');
const credentials = require('../myTestdata');
const Page = require('../../helpers/helpers');
const Wait = require('../../helpers/waiters');
const Assert = require('../../helpers/validators');

describe('should create and delete list', () => {

    before(() => {
        Page.loginWithDefaultUser();
    });

    it('should create list', () => {

        menuSteps.navigateToLists();
        //const allListBeforAdded = pageSteps.getAllLists();
        pageSteps.createNewList();
        pageSteps.enterListName(credentials.listName);
        pageSteps.saveList();

        // assert.strictEqual(allListBeforAdded, pageSteps.getAllLists() - 1);
        // assert.strictEqual(pageSteps.getNameOfAddedList(), credentials.listName);
    });
    xit('should delete list', () => {
        const deleteBtn = $$('button.is-danger')[0];
        deleteBtn.click();
        const aproveDelBtn = $('div.box button.is-danger');
        aproveDelBtn.click();
        browser.pause(1000);
        const isEnabled = deleteBtn.isEnabled();

        assert.equal(isEnabled, false); //перевірка
    });

    after(() => {

        browser.reloadSession();
    })
})