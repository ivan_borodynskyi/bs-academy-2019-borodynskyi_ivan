const pageSteps = require('./Create_new_acc_pa');
const credentials = require('./../myTestdata');
const Page = require('../../helpers/helpers');
const Wait = require('../../helpers/waiters');
const Assert = require('../../helpers/validators');


describe('create new user and invalid cred tests', () => {

    beforeEach(() => {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);
    });

    afterEach(() => {
        browser.reloadSession();
    });

    it('should create new user', () => {

        pageSteps.createNewAcc();
        pageSteps.enterFirstName(credentials.name);
        pageSteps.enterLastName(credentials.surname);
        pageSteps.enterEmail(credentials.newEmailForRegistration);
        pageSteps.enterNewPassword(credentials.password);
        pageSteps.confirmCreation();
        Assert.successNotificationTextIs(credentials.successRegistrationMessage);
    });


    it('should not login with invalid credentials', () => {

        Page.loginWithCustomUser(credentials.notValidname, credentials.notValidEmail);
        Assert.errorNotificationTextIs(credentials.incorrectCredentialsMessage);
    });




});