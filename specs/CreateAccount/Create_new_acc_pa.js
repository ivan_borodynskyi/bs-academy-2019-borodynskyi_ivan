const createNewAccPage = require('./Create_new_acc_po');

const page = new createNewAccPage();

class createNewAccPageActions {

    createNewAcc() {
        page.createNewAcc.waitForDisplayed(2000);
        page.createNewAcc.click();
    }
    enterFirstName(value) {
        page.firstNameInput.waitForDisplayed(2000);
        page.firstNameInput.clearValue();
        page.firstNameInput.setValue(value);
    }

    enterLastName(value) {
        page.lastNameInput.waitForDisplayed(2000);
        page.lastNameInput.clearValue();
        page.lastNameInput.setValue(value);
    }

    enterEmail(value) {
        page.emailInput.waitForDisplayed(2000);
        page.emailInput.clearValue();
        page.emailInput.setValue(value);
    }

    enterNewPassword(value) {
        page.newPasswordInput.waitForDisplayed(2000);
        page.newPasswordInput.setValue(value);
    }

    confirmCreation() {
        page.confirmButton.waitForDisplayed(2000);
        page.confirmButton.click();
    }

}
module.exports = new createNewAccPageActions();