       class createNewAccPage {
           get createNewAcc() { return $('a.link.link-signup') };
           get firstNameInput() { return $('input[name=firstName]') };
           get lastNameInput() { return $('input[name=lastName]') };
           get emailInput() { return $('input[name=email]') };
           get newPasswordInput() { return $('input[type=password]') };
           get confirmButton() { return $('button.is-rounded') };
       }

       module.exports = createNewAccPage;