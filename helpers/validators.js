const assert = require('chai').assert;
const expect = require('chai').expect;

class AssertHelper {



    elementCountIs(locator, expectedQty) {
        const els = locator;
        const actualQty = els.length;

        assert.strictEqual(actualQty, expectedQty, `Expected ${expectedQty} is not equal to ${actualQty}`);
    }

    wrongValueIndicationOnField(locator) {
        const attr = locator.getAttribute('class');
        expect(attr, `${attr} doesn't include validation class`).to.include("is-danger");
    }

    wrongValueIndicationOnLable(locator) {

        const attr = locator.getAttribute('class');
        expect(attr, `${attr} doesn't include error class`).to.include("error");
    }



    errorNotificationTextIs(expectedText) {
        const notification = $('div.toast.is-danger div');
        const actualText = notification.getText()
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }

    successNotificationTextIs(expectedText) {
        const notification = $('div.toast.is-success div');
        const actualText = notification.getText()
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }

    newPlaceIS(expectedText) {
        const placeNameAfterAdd = $('div.place-venue__place-name');
        const actualText = placeNameAfterAdd.getText();
        assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    }

    // newPlaceTagIs(expectedText) {
    //     const tagMilk = $('div.place-sidebar__tags span.tag');
    //     const actualText = tagMilk.getText();
    //     assert.equal(actualText, expectedText, `Expected ${actualText} to be equal to ${expectedText}`);
    // }

}

module.exports = new AssertHelper();